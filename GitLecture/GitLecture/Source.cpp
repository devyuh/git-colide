﻿#include "Header.h"

int main(){
	printf("Hello world\n");

	int* dynamic_array = (int*)malloc(sizeof(int)*DYNAMIC_ARRAY_SIZE);
	for(int i=0; i<DYNAMIC_ARRAY_SIZE; i++){
		dynamic_array[i] = i*(rand()%10);
	}
	for(int i=0; i<DYNAMIC_ARRAY_SIZE; i++){
		printf("dynamic_array[%d] = %d\n", i, dynamic_array[i]);
	}

	printf("원주율 : %.10f\n", calculate_pi());

	return 0;
}

double calculate_sqrt2(){
	return sqrt(2.0);
}

double calculate_pi(){
	return acos(-1.0);
}